package org.flaxxy.sixrr.repository;

import org.flaxxy.sixrr.entity.ServiceOffer;
import org.flaxxy.sixrr.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface ServiceOfferRepository extends CrudRepository<ServiceOffer, Long> {

    @Query("SELECT offer FROM ServiceOffer offer WHERE offer.seller=?1")
    Collection<ServiceOffer> getOffersOfUser(User seller);

    @Query("SELECT offer FROM ServiceOffer offer WHERE (offer.description LIKE %?1%) OR (offer.title LIKE %?1%) OR (offer.seller.username LIKE %?1%)")
    Collection<ServiceOffer> searchOffers(String searchString);
}
