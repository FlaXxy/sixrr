package org.flaxxy.sixrr.repository;

import org.flaxxy.sixrr.entity.ServiceOrder;
import org.flaxxy.sixrr.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface ServiceOrderRepository extends CrudRepository<ServiceOrder, Long> {

    @Query("SELECT order FROM ServiceOrder order WHERE order.buyer=?1")
    Collection<ServiceOrder> getOrdersOfBuyer(User buyer);

    @Query("SELECT order FROM ServiceOrder order WHERE order.offer.seller=?1")
    Collection<ServiceOrder> getOrdersOfSeller(User seller);
}
