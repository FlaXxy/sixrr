package org.flaxxy.sixrr.entity;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import java.time.Instant;

@Entity
public class Revision extends SingleIdEntity<Long> {
    private Instant finishTime;
    @NotNull
    private String sellerComment;
    @NotNull
    private String artifact;

    public Instant getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Instant finishTime) {
        this.finishTime = finishTime;
    }

    public String getSellerComment() {
        return sellerComment;
    }

    public void setSellerComment(String requestDescription) {
        this.sellerComment = requestDescription;
    }

    public String getArtifact() {
        return artifact;
    }

    public void setArtifact(String artifact) {
        this.artifact = artifact;
    }
}
