package org.flaxxy.sixrr.controller;

import org.flaxxy.sixrr.dto.AlertsDTO;
import org.flaxxy.sixrr.dto.NewOfferDTO;
import org.flaxxy.sixrr.entity.User;
import org.flaxxy.sixrr.service.OfferServiceIF;
import org.flaxxy.sixrr.service.UserServiceIF;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class OfferController {

    private final OfferServiceIF offerService;
    private final UserServiceIF userService;

    public OfferController(OfferServiceIF offerService, UserServiceIF userService) {
        this.offerService = offerService;
        this.userService = userService;
    }

    @GetMapping("/search")
    public String search(Model model, @RequestParam("q") String searchString) {
        model.addAttribute("searchString", searchString);
        var result = offerService.searchOffers(searchString);
        model.addAttribute("searchResult", result);
        return "search";
    }

    @GetMapping("/offers/{id}")
    public String offers(Model model, @PathVariable long id) {
        var offer = offerService.getOffer(id);
        model.addAttribute("offer", offer);
        return "offer";
    }

    @GetMapping("/newOffer")
    public String newOffer(Model model, @AuthenticationPrincipal User principal) {
        model.addAttribute("offer", new NewOfferDTO());
        var paymentMethods = userService.getUser(principal.getId()).getPaymentMethods();
        if (paymentMethods.isEmpty()) {
            model.addAttribute("alerts", new AlertsDTO().addError("No Payment Method found"));
            return "index";
        }
        model.addAttribute("paymentMethods", paymentMethods);
        return "newOffer";
    }

    @PostMapping("/offers")
    public String newOffer(Model model, @AuthenticationPrincipal User user, @ModelAttribute("offer") @Valid NewOfferDTO offerDTO, BindingResult result) {
        if (result.hasErrors()) {
            model.addAttribute("offer", offerDTO);
            var paymentMethods = userService.getUser(user.getId()).getPaymentMethods();
            if (paymentMethods.isEmpty()) {
                model.addAttribute("alerts", new AlertsDTO().addError("No Payment Method found"));
                return "index";
            }
            model.addAttribute("paymentMethods", paymentMethods);
            return "newOffer";
        }

        var offer = offerService.addOffer(user, offerDTO);
        if (offer == null) {
            model.addAttribute("alerts", new AlertsDTO().addError("Error creating the offer."));
            return newOffer(model, user);
        }


        return "redirect:/offers/" + offer.getId();
    }
}
