package org.flaxxy.sixrr.service;


import org.flaxxy.sixrr.dto.NewOfferDTO;
import org.flaxxy.sixrr.entity.ServiceOffer;
import org.flaxxy.sixrr.entity.User;

import javax.transaction.Transactional;
import java.util.Collection;

public interface OfferServiceIF {

    @Transactional
    Collection<ServiceOffer> searchOffers(String searchString);

    @Transactional
    ServiceOffer addOffer(User user, NewOfferDTO offerDTO);

    @Transactional
    ServiceOffer getOffer(long id);

    @Transactional
    Collection<ServiceOffer> getOffersOfUser(User sellerDTO);

    @Transactional
    Iterable<ServiceOffer> getRandomOffers();
}
