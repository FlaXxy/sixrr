package org.flaxxy.sixrr.config;

import org.flaxxy.sixrr.service.UserService;
import org.flaxxy.sixrr.utils.SecurityUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private static final String[] ALLOW_ACCESS_WITHOUT_AUTHENTICATION = {
            "/login", "/forgotPassword", "/signup", "/api/**"};
    private static final String[] ALLOW_READ_ACCESS_WITHOUT_AUTHENTICATION = {"/css/**", "/image/**", "/fonts/**", "/", "/offers/**", "/search", "/users/**"};

    private final UserService userDetailsService;
    private final SecurityUtils securityUtils;

    public WebSecurityConfig(UserService userDetailsService, SecurityUtils securityUtils) {
        this.userDetailsService = userDetailsService;
        this.securityUtils = securityUtils;
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.authorizeRequests()
                .antMatchers(ALLOW_ACCESS_WITHOUT_AUTHENTICATION).permitAll()
                .antMatchers(HttpMethod.GET, ALLOW_READ_ACCESS_WITHOUT_AUTHENTICATION).permitAll()
                .anyRequest().authenticated();
        http.formLogin()
                .loginPage("/login")
                .defaultSuccessUrl("/")
                .failureUrl("/login?error")
                .permitAll();
        http.logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/?logout")
                .deleteCookies("remember-me")
                .permitAll();
        http.rememberMe();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(securityUtils.getEncoder());
    }

    @Override
    public UserDetailsService userDetailsService() {
        return userDetailsService;
    }

}
