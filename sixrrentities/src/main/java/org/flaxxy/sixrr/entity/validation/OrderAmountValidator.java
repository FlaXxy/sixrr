package org.flaxxy.sixrr.entity.validation;

import org.flaxxy.sixrr.entity.ServiceOrder;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class OrderAmountValidator implements ConstraintValidator<OrderAmountConstraint, ServiceOrder> {
    @Override
    public void initialize(OrderAmountConstraint constraint) {
    }

    @Override
    public boolean isValid(ServiceOrder obj, ConstraintValidatorContext context) {
        var offer = obj.getOffer();
        if (offer == null)
            return false;
        return offer.getMaxUnits() >= obj.getAmount();
    }
}
