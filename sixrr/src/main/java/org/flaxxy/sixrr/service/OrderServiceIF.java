package org.flaxxy.sixrr.service;

import org.flaxxy.sixrr.dto.NewOrderDTO;
import org.flaxxy.sixrr.entity.Revision;
import org.flaxxy.sixrr.entity.ServiceOrder;
import org.flaxxy.sixrr.entity.User;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Optional;

public interface OrderServiceIF {
    @Transactional
    ServiceOrder orderService(User buyer, NewOrderDTO orderDTO);

    @Transactional
    Revision provideRevision(ServiceOrder order, Revision revisionDTO);

    @Transactional
    void acceptRevision(ServiceOrder orderDTO);

    @Transactional
    void rejectRevision(ServiceOrder orderDTO);

    @Transactional
    Collection<ServiceOrder> getOrdersOfBuyer(User seller);

    @Transactional
    Optional<ServiceOrder> getOrder(long orderId);

    @Transactional
    Collection<ServiceOrder> getOrdersOfSeller(User seller);

    @Transactional
    boolean canAddRevision(ServiceOrder order);
}
