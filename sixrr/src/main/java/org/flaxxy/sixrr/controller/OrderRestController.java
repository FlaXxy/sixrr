package org.flaxxy.sixrr.controller;

import org.flaxxy.sixrr.dto.NewOrderDTO;
import org.flaxxy.sixrr.entity.ServiceOffer;
import org.flaxxy.sixrr.service.OfferServiceIF;
import org.flaxxy.sixrr.service.OrderServiceIF;
import org.flaxxy.sixrr.service.UserServiceIF;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
public class OrderRestController {
    private final OfferServiceIF offerService;
    private final UserServiceIF userService;
    private final OrderServiceIF orderService;

    public OrderRestController(OfferServiceIF offerService, UserServiceIF userService, OrderServiceIF orderService) {
        this.offerService = offerService;
        this.userService = userService;
        this.orderService = orderService;
    }

    @GetMapping("/api/searchOffers")
    public Collection<ServiceOffer> getKathiOffers(@RequestParam("query") String query) {
        var offers = offerService.searchOffers(query);
        //don't send users, because they contain sensitive data
        offers.forEach(o -> {
            o.setSeller(null);
            o.setPaymentMethod(null);
        });
        return offers;
    }

    @PostMapping("/api/orders")
    public boolean newOrder(@RequestBody NewOrderDTO newOrderDTO,
                            @RequestParam("username") String username, @RequestParam("password") String password) {
        var user = userService.getAuthenticatedUser(username, password);
        if (user == null) {
            return false;
        }
        var pm = user.getPaymentMethods().stream().findFirst();
        if (pm.isEmpty()) {
            return false;
        }

        newOrderDTO.setPaymentMethodId(pm.get().getId());
        var offer = orderService.orderService(user, newOrderDTO);
        return offer != null;
    }

}
