package org.flaxxy.sixrr.utils;

public class SixrrException extends Exception {
    public SixrrException(String message) {
        super(message);
    }
}
