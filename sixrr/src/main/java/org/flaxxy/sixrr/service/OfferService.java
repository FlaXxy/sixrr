package org.flaxxy.sixrr.service;

import org.flaxxy.sixrr.dto.NewOfferDTO;
import org.flaxxy.sixrr.entity.ServiceOffer;
import org.flaxxy.sixrr.entity.User;
import org.flaxxy.sixrr.repository.ServiceOfferRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class OfferService implements OfferServiceIF {
    private static final int MAX_SEARCH_KEYWORDS = 10;
    private final ServiceOfferRepository repository;
    private final UserServiceIF userService;

    public OfferService(ServiceOfferRepository repository, UserServiceIF userService) {
        this.repository = repository;
        this.userService = userService;
    }

    @Override
    @Transactional
    public Collection<ServiceOffer> searchOffers(String searchString) {
        if (searchString == null || searchString.isBlank()) {
            return Collections.emptyList();
        }
        var matches = Stream.<ServiceOffer>empty();
        for (var keyword : searchString.split(" ", MAX_SEARCH_KEYWORDS)) {
            matches = Stream.concat(matches, repository.searchOffers(keyword).parallelStream());
        }
        var occurences = matches.collect(Collectors.groupingBy(o -> o, Collectors.counting()));
        return occurences
                .entrySet()
                .parallelStream()
                .sorted(Comparator.comparingLong(Map.Entry::getValue))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public ServiceOffer addOffer(User user, NewOfferDTO offerDTO) {
        user = userService.getUser(user.getId());
        var offer = new ServiceOffer();
        offer.setSeller(user);
        offer.setShortDescription(offerDTO.getShortDescription());
        offer.setDescription(offerDTO.getDescription());
        offer.setTitle(offerDTO.getTitle());

        var paymentMethod = user.getPaymentMethods().stream().filter(pm -> pm.getId() == offerDTO.getPaymentMethodId()).findFirst().orElseThrow();

        offer.setPaymentMethod(paymentMethod);

        offer.setMaxRevisions(offerDTO.getMaxRevisions());
        offer.setPrice(offerDTO.getPrice());
        offer.setMaxUnits(offerDTO.getMaxUnits());
        offer.setInvoiceUnit(offerDTO.getInvoiceUnit());
        offer.setDurationPerUnit(offerDTO.getDurationPerUnit());
        offer.setDuration(offerDTO.getDuration());
        offer.setPricePerUnit(offerDTO.getPricePerUnit());

        return repository.save(offer);
    }

    @Override
    @Transactional
    public ServiceOffer getOffer(long id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public Collection<ServiceOffer> getOffersOfUser(User sellerDTO) {
        return repository.getOffersOfUser(sellerDTO);
    }

    @Override
    @Transactional
    public Iterable<ServiceOffer> getRandomOffers() {
        return repository.findAll();
    }
}
