package org.flaxxy.sixrr.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Objects;

@MappedSuperclass
public abstract class SingleIdEntity<K> implements Serializable {

    @Id
    @GeneratedValue
    private K id;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SingleIdEntity user = (SingleIdEntity) o;
        return Objects.equals(id, user.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }


    public K getId() {
        return id;
    }

    public void setId(K id) {
        this.id = id;
    }
}
