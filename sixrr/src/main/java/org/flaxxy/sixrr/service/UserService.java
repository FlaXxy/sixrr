package org.flaxxy.sixrr.service;

import org.flaxxy.sixrr.entity.PaymentMethod;
import org.flaxxy.sixrr.entity.User;
import org.flaxxy.sixrr.repository.UserRepository;
import org.flaxxy.sixrr.utils.SecurityUtils;
import org.flaxxy.sixrr.utils.SixrrException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;

@Service
public class UserService implements UserDetailsService, UserServiceIF {
    private final UserRepository userRepository;
    private final SecurityUtils securityUtils;

    public UserService(UserRepository userRepository, SecurityUtils securityUtils) {
        this.userRepository = userRepository;
        this.securityUtils = securityUtils;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        var user = findByUsername(username);
        if (user != null)
            return user;
        user = findByEmail(username);
        if (user != null)
            return user;
        throw new UsernameNotFoundException("User not Found in the Database");
    }

    @Override
    @Transactional
    public User findByEmail(String mail) {
        var res = userRepository.findByEmail(mail.toLowerCase());
        Assert.isTrue(res.size() <= 1, "Got more than one result from the database");
        if (res.size() == 0)
            return null;
        return res.iterator().next();
    }

    @Override
    @Transactional
    public User findByUsername(String name) {
        var res = userRepository.findByUsername(name.toLowerCase());
        Assert.isTrue(res.size() <= 1, "Got more than one result from the database");
        if (res.size() == 0)
            return null;
        return res.stream().findFirst().get();
    }

    @Override
    @Transactional
    public User getUser(long id) {
        return userRepository.findById(id).orElse(null);
    }


    @Override
    @Transactional
    public User createUser(User user) {
        //ensure, new user is created
        user.setId(0L);
        user.setPassword(securityUtils.getEncoder().encode(user.getPassword()));
        return userRepository.save(user);
    }

    @Override
    @Transactional
    public User getAuthenticatedUser(String username, String password) {
        var user = findByUsername(username);
        if (user == null)
            return null;
        if (!securityUtils.getEncoder().matches(password, user.getPassword()))
            return null;
        return user;
    }

    @Override
    @Transactional
    public void updateProfile(User user, User data) {
        //user may be an old version
        user = userRepository.findById(user.getId()).orElseThrow();

        //update the password?
        if (data.getPassword() != null) {
            user.setPassword(data.getPassword());
        }
        user.setAddress(data.getAddress());
        user.setFirstName(data.getFirstName());
        user.setLastName(data.getLastName());
        user.setEmail(data.getEmail());
        user.setSellerDescription(data.getSellerDescription());
    }

    @Override
    @Transactional
    public void addPaymentMethod(User user, PaymentMethod paymentMethod) {
        //user may be an old version
        user = userRepository.findById(user.getId()).orElseThrow();
        user.getPaymentMethods().add(paymentMethod);
    }

    @Override
    @Transactional
    public void removePaymentMethod(User user, long paymentMethodId) throws SixrrException {
        //user may be an old version
        user = userRepository.findById(user.getId()).orElseThrow();
        var methods = user.getPaymentMethods();
        if (methods.size() <= 1) {
            throw new SixrrException("Can't remove the last paymentMethod");
        }
        user.getPaymentMethods().removeIf(p -> p.getId() == paymentMethodId);
    }

}
