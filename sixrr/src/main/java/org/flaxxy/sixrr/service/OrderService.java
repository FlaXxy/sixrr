package org.flaxxy.sixrr.service;

import de.klamer.BambooForestStore.dto.OrderDto;
import de.klamer.BambooForestStore.dto.OrderItemDto;
import de.klamer.BambooForestStore.dto.ProductVariationDto;
import org.flaxxy.sixrr.dto.NewOrderDTO;
import org.flaxxy.sixrr.entity.*;
import org.flaxxy.sixrr.repository.RevisionRepository;
import org.flaxxy.sixrr.repository.ServiceOfferRepository;
import org.flaxxy.sixrr.repository.ServiceOrderRepository;
import org.flaxxy.sixrr.repository.UserRepository;
import org.flaxxy.sixrr.service.payment.PaymentProviderIF;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static org.flaxxy.sixrr.config.CommunicationConfig.*;

@Service
public class OrderService implements OrderServiceIF {

    private final ServiceOrderRepository orderRepository;
    private final ServiceOfferRepository offerRepository;
    private final RevisionRepository revisionRepository;
    private final UserRepository userRepository;
    private final RestTemplate restTemplate;
    private final HashMap<PaymentSite, PaymentProviderIF> paymentProviders;
    private final BigDecimal sellerShare = BigDecimal.valueOf(0.8);

    public OrderService(ServiceOrderRepository orderRepository, ServiceOfferRepository offerRepository, RevisionRepository revisionRepository,
                        UserRepository userRepository, RestTemplate restTemplate, @Qualifier("paymentProviders") HashMap<PaymentSite, PaymentProviderIF> paymentProviders) {
        this.orderRepository = orderRepository;
        this.offerRepository = offerRepository;
        this.revisionRepository = revisionRepository;
        this.userRepository = userRepository;
        this.restTemplate = restTemplate;
        this.paymentProviders = paymentProviders;
    }

    @Override
    @Transactional
    public ServiceOrder orderService(User buyer, NewOrderDTO newOrderDTO) {
        Collection<ProductVariationDto> bambooOrders = newOrderDTO.getBambooOrders() == null ? Collections.emptyList() : Arrays.stream(newOrderDTO.getBambooOrders()).mapToObj(orderId ->
                restTemplate.getForObject(BAMBOOFORESTSTORE_ADDRESS + "/api/productVariation/{id}", ProductVariationDto.class, orderId)).collect(Collectors.toList());

        //gather data
        var bambooOrderItems = bambooOrders.stream()
                .map(bambooOrder -> new OrderItemDto(bambooOrder.getId().toString(), "1")).collect(Collectors.toSet());
        var offer = offerRepository.findById(newOrderDTO.getOfferId()).orElseThrow();
        buyer = userRepository.findById(buyer.getId()).orElseThrow();
        var buyerPaymentMethod = buyer.getPaymentMethods().stream().filter(pm -> pm.getId() == newOrderDTO.getPaymentMethodId()).findFirst().orElseThrow();
        var buyerpaymentProvider = paymentProviders.get(buyerPaymentMethod.getSite());
        var sellerPaymentMethod = offer.getPaymentMethod();
        var myPaymentProvider = paymentProviders.get(PaymentSite.bogerpay);

        //create the order
        var order = new ServiceOrder();
        order.setStatus(OrderStatus.WaitingForRevision);
        order.setOffer(offer);
        order.setBuyer(buyer);
        order.setPaymentMethod(buyerPaymentMethod);
        order.setAmount(newOrderDTO.getAmount());
        order.setRequestDescription(newOrderDTO.getRequestDescription());
        order.setBambooOrderInfo(bambooOrderItems.isEmpty() ? "" : bambooOrderItems.stream().map(item -> item.getProductVarId() + ", ").reduce("BambooOrderIds: ", String::concat));

        //paying
        var price = offer.getPrice().add(offer.getPricePerUnit().multiply(BigDecimal.valueOf(newOrderDTO.getAmount())));
        var bambooPrice = bambooOrders.stream().map(ProductVariationDto::getNetPrice).reduce(BigDecimal::add);
        var totalPrice = price;
        var sendToSeller = sellerShare.multiply(price);

        if (bambooPrice.isPresent()) {
            totalPrice = totalPrice.add(bambooPrice.get());
            //transport fee of bambooShop
            totalPrice = totalPrice.add(BigDecimal.valueOf(6));
        }
        var reference = String.format("Sixrr payment for %s %s", order.getOffer().getTitle(), order.getBambooOrderInfo());

        var username = buyerPaymentMethod.getUsername();
        var password = buyerPaymentMethod.getPassword();
        var iban = buyerPaymentMethod.getIban();

        boolean hasPayed = false;
        try {
            hasPayed = buyerpaymentProvider.send(username, password, iban, MY_BOGERPAY_IBAN, totalPrice, reference, restTemplate);
        } catch (Exception e) {
            Logger.getGlobal().log(Level.WARNING, e.toString());
        }

        if (!hasPayed) {
            Logger.getGlobal().log(Level.WARNING, "cannot pay");
            return null;
        }

        //sending share to seller
        var sellerIban = sellerPaymentMethod.getIban();
        try {
            myPaymentProvider.send(MY_BOGERPAY_USERNAME, MY_BOGERPAY_PASSWORD, MY_BOGERPAY_IBAN, sellerIban, sendToSeller, reference, restTemplate);
        } catch (Exception ignored) {
            //I don't handle errors here, because I can't handle a partionally successful distributed transaction.
        }

        if (!bambooOrders.isEmpty()) {
//            var buyerAddress = buyer.getAddress();
//            var address = new de.klamer.BambooForestStore.entity.Address(buyerAddress.getStreet(), buyerAddress.getHouse(),
//                    buyerAddress.getTown(), "", buyerAddress.getCountry());


            var orderDTO = new OrderDto(new de.klamer.BambooForestStore.entity.Address(buyer.getFirstName() + " " + buyer.getLastName(), "Street", "69", "Darmstadt", "42069", "Schland"), bambooOrderItems);
            restTemplate.postForObject(BAMBOOFORESTSTORE_ADDRESS + "/api/order?email={email}&password={password}", orderDTO, Boolean.class, BAMBOO_USERNAME, BAMBOO_PASSWORD);
        }
        //        restTemplate.postForObject("http://192.168.0.4:8080/api/order?email={email}&password={password}",);

        //persist
        return orderRepository.save(order);
    }

    @Override
    @Transactional
    public Revision provideRevision(ServiceOrder order, Revision revision) {
        order = orderRepository.findById(order.getId()).orElseThrow();

        if (order.getRevisions().size() >= order.getOffer().getMaxRevisions())
            return null;

        if (order.getStatus() != OrderStatus.WaitingForRevision) {
            return null;
        }

        revision.setFinishTime(Instant.now());
        revision.setId(0L);
        revision = revisionRepository.save(revision);
        order.getRevisions().add(revision);
        if (order.getRevisions().size() >= order.getOffer().getMaxRevisions()) {
            order.setStatus(OrderStatus.Completed);
        } else {
            order.setStatus(OrderStatus.WaitingForAccept);
        }
        return revision;
    }

    @Override
    @Transactional
    public void acceptRevision(ServiceOrder order) {
        order = orderRepository.findById(order.getId()).orElseThrow();
        if (order.getStatus() != OrderStatus.WaitingForAccept) {
            return;
        }
        order.setStatus(OrderStatus.Completed);
    }

    @Override
    @Transactional
    public void rejectRevision(ServiceOrder order) {
        order = orderRepository.findById(order.getId()).orElseThrow();
        if (order.getStatus() != OrderStatus.WaitingForAccept) {
            return;
        }
        order.setStatus(OrderStatus.WaitingForRevision);
    }

    @Override
    @Transactional
    public Collection<ServiceOrder> getOrdersOfBuyer(User seller) {
        return orderRepository.getOrdersOfBuyer(seller);
    }

    @Override
    @Transactional
    public Optional<ServiceOrder> getOrder(long orderId) {
        return orderRepository.findById(orderId);
    }

    @Override
    @Transactional
    public Collection<ServiceOrder> getOrdersOfSeller(User seller) {
        return orderRepository.getOrdersOfSeller(seller);
    }

    @Override
    @Transactional
    public boolean canAddRevision(ServiceOrder order) {
        var revisions = order.getRevisions();
        return revisions == null || (revisions.size() < order.getOffer().getMaxRevisions() && order.getStatus() == OrderStatus.WaitingForRevision);
    }

}
