package org.flaxxy.sixrr.config;

public class CommunicationConfig {
    public static final String MY_BOGERPAY_IBAN = "DE50750500000000000004";
    public static final String MY_BOGERPAY_USERNAME = "richter@bp.de";
    public static final String MY_BOGERPAY_PASSWORD = "1234";
    public static final String BAMBOO_USERNAME = "felix@felix.de";
    public static final String BAMBOO_PASSWORD = "felix";
    public static final String BOGERPAY_ADDRESS = "http://im-codd:8856";
    public static final String BAMBOOFORESTSTORE_ADDRESS = "http://im-codd:8842";
//    public static final String BOGERPAY_ADDRESS = "http://192.168.178.40:8856";
//    public static final String BAMBOOFORESTSTORE_ADDRESS = "http://192.168.178.41:8842";

    private CommunicationConfig() {
    }

}
