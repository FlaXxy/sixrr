package org.flaxxy.sixrr.dto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class AlertsDTO {

    private final List<String> errors = new ArrayList<>();
    private final List<String> notifications = new ArrayList<>();

    public AlertsDTO addError(String error) {
        errors.add(error);
        return this;
    }

    public AlertsDTO addNotification(String notification) {
        notifications.add(notification);
        return this;
    }

    public Collection<String> getNotifications() {
        return notifications;
    }

    public Collection<String> getErrors() {
        return errors;
    }

}
