package org.flaxxy.sixrr.repository;

import org.flaxxy.sixrr.entity.Revision;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RevisionRepository extends CrudRepository<Revision, Long> {

}
