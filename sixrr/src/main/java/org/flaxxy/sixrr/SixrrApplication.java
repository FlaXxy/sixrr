package org.flaxxy.sixrr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SixrrApplication {

	public static void main(String[] args) {
		SpringApplication.run(SixrrApplication.class, args);
	}

}
