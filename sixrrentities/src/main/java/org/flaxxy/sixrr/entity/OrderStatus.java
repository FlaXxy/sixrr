package org.flaxxy.sixrr.entity;

public enum OrderStatus {
    WaitingForRevision,
    WaitingForAccept,
    Completed
}
