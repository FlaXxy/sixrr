package org.flaxxy.sixrr.utils;

import org.flaxxy.sixrr.dto.NewOfferDTO;
import org.flaxxy.sixrr.entity.PaymentMethod;
import org.flaxxy.sixrr.entity.PaymentSite;
import org.flaxxy.sixrr.entity.User;
import org.flaxxy.sixrr.service.OfferServiceIF;
import org.flaxxy.sixrr.service.OrderServiceIF;
import org.flaxxy.sixrr.service.UserServiceIF;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.Duration;
import java.util.HashSet;

@Component
public class EventCallbacks {

    private final OfferServiceIF offerService;
    private final OrderServiceIF orderService;
    private final UserServiceIF userService;

    public EventCallbacks(OfferServiceIF offerService, OrderServiceIF orderService, UserServiceIF userService) {
        this.offerService = offerService;
        this.orderService = orderService;
        this.userService = userService;
    }

    //This method needs to be transactional, because self calls can't be intercepted to start a transaction...
    @EventListener(ApplicationReadyEvent.class)
    @Transactional
    public void fillWithData() {
        if (userService.findByUsername("admin") != null) {
            //we are probably already initialized.
            return;
        }
        fillUsers();
        fillOffers();
    }

    protected void fillOffers() {
        {
            var offer = new NewOfferDTO();
            offer.setTitle("Pat on the head");
            offer.setDescription("I will pat you on the head, because you were a good boye!\uD83D\uDC36\nProfessional patter.\nBig hands.");
            offer.setShortDescription("I will pat you on the head, because you were a good boye!\uD83D\uDC36");
            offer.setMaxRevisions(5);

            offer.setPrice(BigDecimal.valueOf(69));
            offer.setDuration(Duration.ofMinutes(15));

            offer.setInvoiceUnit("Pats");
            offer.setMaxUnits(100);
            offer.setDurationPerUnit(Duration.ofMinutes(1));
            offer.setPricePerUnit(BigDecimal.valueOf(4.20));
            var admin = userService.findByUsername("admin");

            offer.setPaymentMethodId(admin.getPaymentMethods().stream().findFirst().orElseThrow().getId());

            offerService.addOffer(admin, offer);
        }
        {
            var offer = new NewOfferDTO();
            offer.setTitle("Transport problematic packets");
            offer.setDescription("I will transport your extra big/heavy/breakable package anywhere, even to the top of the Himalaya. #transport #special");
            offer.setShortDescription("Transport problematic packets anywhere.");
            offer.setMaxRevisions(1);

            offer.setPrice(BigDecimal.valueOf(0));
            offer.setDuration(Duration.ofDays(3));

            offer.setInvoiceUnit("Kilometers");
            offer.setMaxUnits(1000);
            offer.setDurationPerUnit(Duration.ofMinutes(1));
            offer.setPricePerUnit(BigDecimal.valueOf(0.5));

            var user = userService.findByUsername("user");

            offer.setPaymentMethodId(user.getPaymentMethods().stream().findFirst().orElseThrow().getId());

            offerService.addOffer(user, offer);
        }
        {
            var offer = new NewOfferDTO();
            offer.setTitle("Express Transport");
            offer.setDescription("Super fast express transportation of a packet, delivery on the next day guaranteed. #transport #express");
            offer.setShortDescription("Express Transport anywhere.");
            offer.setMaxRevisions(1);

            offer.setPrice(BigDecimal.valueOf(0));
            offer.setDuration(Duration.ofDays(1));

            offer.setInvoiceUnit("Kilometers");
            offer.setMaxUnits(1000);
            offer.setDurationPerUnit(Duration.ofMinutes(0));
            offer.setPricePerUnit(BigDecimal.valueOf(0.25));

            var user = userService.findByUsername("user");

            offer.setPaymentMethodId(user.getPaymentMethods().stream().findFirst().orElseThrow().getId());

            offerService.addOffer(user, offer);
        }
    }

    protected void fillUsers() {
        {
            var greimUser = new User();
            greimUser.setUsername("greim");
            greimUser.setPassword("greim");
            greimUser.setFirstName("Katharina");
            greimUser.setLastName("Greim");
            greimUser.setEmail("greim@greim.com");
            greimUser.setSellerRating(3);

            var pm = new PaymentMethod();
            pm.setPassword("1234");
            pm.setUsername("greim@bp.de");
            pm.setIban("DE50750500000000000003");
            pm.setSite(PaymentSite.bogerpay);

            var pms = new HashSet<PaymentMethod>();
            pms.add(pm);
            greimUser.setPaymentMethods(pms);
            userService.createUser(greimUser);
        }
        {
            var admin = new User();
            admin.setUsername("admin");
            admin.setPassword("admin");
            admin.setFirstName("ad");
            admin.setLastName("min");
            admin.setEmail("admin@admin.com");
            admin.setSellerRating(3);

            var pm = new PaymentMethod();
            pm.setPassword("1234");
            pm.setUsername("greim@bp.de");
            pm.setIban("DE50750500000000000003");
            pm.setSite(PaymentSite.bogerpay);

            var pms = new HashSet<PaymentMethod>();
            pms.add(pm);
            admin.setPaymentMethods(pms);
            userService.createUser(admin);
        }
        {
            var user = new User();
            user.setUsername("user");
            user.setPassword("user");
            user.setFirstName("us");
            user.setLastName("er");
            user.setEmail("user@user.com");

            var pm = new PaymentMethod();
            pm.setPassword("1234");
            pm.setUsername("klamer@bp.de");
            pm.setIban("DE50750500000000000002");
            pm.setSite(PaymentSite.bogerpay);

            var pms = new HashSet<PaymentMethod>();
            pms.add(pm);
            user.setPaymentMethods(pms);
            user.setSellerRating(3);
            userService.createUser(user);
        }
    }
}
