package org.flaxxy.sixrr.service.payment;

import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;

public interface PaymentProviderIF {
    boolean send(String username, String password, String senderIban, String receiverIban, BigDecimal amount, String reference, RestTemplate restTemplate);
}
