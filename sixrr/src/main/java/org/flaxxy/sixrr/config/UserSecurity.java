package org.flaxxy.sixrr.config;

import org.flaxxy.sixrr.entity.User;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
public class UserSecurity {
    public boolean hasId(Authentication authentication, long userId) {
        var principal = (User) authentication.getPrincipal();
        return principal != null && principal.getId() == userId;
    }
}
