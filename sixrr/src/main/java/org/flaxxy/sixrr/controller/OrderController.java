package org.flaxxy.sixrr.controller;

import de.klamer.BambooForestStore.dto.ProductVariationDto;
import org.flaxxy.sixrr.dto.AlertsDTO;
import org.flaxxy.sixrr.dto.NewOrderDTO;
import org.flaxxy.sixrr.entity.OrderStatus;
import org.flaxxy.sixrr.entity.Revision;
import org.flaxxy.sixrr.entity.User;
import org.flaxxy.sixrr.service.OfferServiceIF;
import org.flaxxy.sixrr.service.OrderServiceIF;
import org.flaxxy.sixrr.service.UserServiceIF;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static org.flaxxy.sixrr.config.CommunicationConfig.BAMBOOFORESTSTORE_ADDRESS;

@Controller
public class OrderController {

    private final OfferServiceIF offerService;
    private final UserServiceIF userService;
    private final OrderServiceIF orderService;
    private final RestTemplate restTemplate;


    public OrderController(OfferServiceIF offerService, UserServiceIF userService, OrderServiceIF orderService, RestTemplate restTemplate) {
        this.offerService = offerService;
        this.userService = userService;
        this.orderService = orderService;
        this.restTemplate = restTemplate;
    }

    @GetMapping("/newOrder")
    public String newOrder(Model model, @RequestParam long id, @AuthenticationPrincipal User user) {
        var offer = offerService.getOffer(id);
        if (offer == null) {
            model.addAttribute("alerts", new AlertsDTO().addError("Offer " + id + " not found!"));
            return "index";
        }

        var paymentMethods = userService.getUser(user.getId()).getPaymentMethods();
        if (paymentMethods.size() == 0) {
            model.addAttribute("alerts", new AlertsDTO().addError("No Payment Method found"));
            return "index";
        }

        ProductVariationDto[] bambooOffers = new ProductVariationDto[0];
        try {
            bambooOffers = restTemplate.getForObject(BAMBOOFORESTSTORE_ADDRESS + "/api/search?query={title}&quantity=3", ProductVariationDto[].class, offer.getTitle());
        } catch (Exception e) {
            Logger.getGlobal().log(Level.WARNING, "Couldn't get bamboo products");
            //e.printStackTrace();
        }
        model.addAttribute("bambooOffers", bambooOffers);

        var order = new NewOrderDTO(id);
        model.addAttribute("order", order);
        model.addAttribute("offer", offer);
        model.addAttribute("paymentMethods", paymentMethods);
        return "newOrder";
    }

    @PostMapping("/orders")
    public String newOrder(Model model, @ModelAttribute("order") @Valid NewOrderDTO orderDTO, @AuthenticationPrincipal User user, BindingResult result) {
        //Todo: confirmation
        if (result.hasErrors()) {
            model.addAttribute("order", orderDTO);
            return "newOrder";
        }

        var order = orderService.orderService(user, orderDTO);

        if (order == null) {
            model.addAttribute("alerts", new AlertsDTO().addError("Error ordering the Service"));
            return newOrder(model, orderDTO.getOfferId(), user);
        }
        return "redirect:/orders/" + order.getId();
    }

    @GetMapping("/orders/{id}")
    public String order(Model model, @PathVariable long id, @AuthenticationPrincipal User user) {
        var optOrder = orderService.getOrder(id);
        if (optOrder.isEmpty()) {
            model.addAttribute("alerts", new AlertsDTO().addError("Order not found"));
            return "index";
        }

        var order = optOrder.get();
        var isBuyer = order.getBuyer().equals(user);
        var isSeller = order.getOffer().getSeller().equals(user);
        if (!isSeller && !isBuyer) {
            model.addAttribute("alerts", new AlertsDTO().addError("Order not found"));
            return "index";
        }

        model.addAttribute("user", user);
        model.addAttribute("order", order);
        Collection<Revision> revisions = order.getRevisions();
        model.addAttribute("revisions", revisions.stream().sorted(Comparator.comparing(Revision::getFinishTime).reversed()).collect(Collectors.toList()));

        if (isSeller && orderService.canAddRevision(order))
            model.addAttribute("newRevisionDTO", new Revision());
        if (isBuyer && order.getStatus() == OrderStatus.WaitingForAccept) {
            model.addAttribute("canAcceptRevision", true);
        }
        return "order";
    }

    @GetMapping("/orders")
    public String orders(Model model, @AuthenticationPrincipal User user) {
        var orders = orderService.getOrdersOfBuyer(user);
        model.addAttribute("orders", orders);
        return "orders";
    }

    @GetMapping("/tasks")
    public String tasks(Model model, @AuthenticationPrincipal User user) {
        var tasks = orderService.getOrdersOfSeller(user);
        model.addAttribute("tasks", tasks);
        return "tasks";
    }

    @PostMapping("/orders/{orderId}/revisions")
    public String provideRevision(Model model, @PathVariable("orderId") long orderId, @ModelAttribute("revisionDTO") @Valid Revision revisionDTO, @AuthenticationPrincipal User user) {
        var optOrder = orderService.getOrder(orderId);
        if (optOrder.isEmpty()) {
            model.addAttribute("alerts", new AlertsDTO().addError("No Order found"));
            return "index";
        }

        var order = optOrder.get();
        var isSeller = order.getOffer().getSeller().equals(user);
        if (!isSeller) {
            model.addAttribute("alerts", new AlertsDTO().addError("Order not found"));
            return "index";
        }
        orderService.provideRevision(order, revisionDTO);
        return "redirect:/orders/" + orderId;
    }

    @PostMapping("/orders/{orderId}/accept")
    public String acceptRevision(Model model, @PathVariable("orderId") long orderId, @AuthenticationPrincipal User user) {
        var optOrder = orderService.getOrder(orderId);
        if (optOrder.isEmpty()) {
            model.addAttribute("alerts", new AlertsDTO().addError("No Order found"));
            return "index";
        }

        var order = optOrder.get();
        var isBuyer = order.getBuyer().equals(user);
        if (!isBuyer) {
            model.addAttribute("alerts", new AlertsDTO().addError("Order not found"));
            return "index";
        }

        orderService.acceptRevision(order);
        return "redirect:/orders/" + orderId;
    }

    @PostMapping("/orders/{orderId}/reject")
    public String rejectRevision(Model model, @PathVariable("orderId") long orderId, @AuthenticationPrincipal User user) {
        var optOrder = orderService.getOrder(orderId);
        if (optOrder.isEmpty()) {
            model.addAttribute("alerts", new AlertsDTO().addError("No Order found"));
            return "index";
        }

        var order = optOrder.get();
        var isBuyer = order.getBuyer().equals(user);
        if (!isBuyer) {
            model.addAttribute("alerts", new AlertsDTO().addError("Order not found"));
            return "index";
        }

        orderService.rejectRevision(order);
        return "redirect:/orders/" + orderId;
    }
}
