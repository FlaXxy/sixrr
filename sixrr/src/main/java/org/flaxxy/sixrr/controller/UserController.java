package org.flaxxy.sixrr.controller;

import org.flaxxy.sixrr.dto.AlertsDTO;
import org.flaxxy.sixrr.entity.PaymentMethod;
import org.flaxxy.sixrr.entity.User;
import org.flaxxy.sixrr.service.OfferServiceIF;
import org.flaxxy.sixrr.service.UserServiceIF;
import org.flaxxy.sixrr.utils.SixrrException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class UserController {

    public static final int MIN_PASSWORD_LENGTH = 6;
    private final UserServiceIF userService;
    private final OfferServiceIF offerService;

    public UserController(UserServiceIF userService, OfferServiceIF offerService) {
        this.userService = userService;
        this.offerService = offerService;
    }

    @GetMapping("/signup")
    public String signup(Model model) {
        model.addAttribute("user", new User());
        return "signup";
    }

    private void verifyMail(User user, BindingResult result) {
        if (userService.findByEmail(user.getEmail()) != null) {
            result.addError(new FieldError("user", "email", "Email already taken!"));
        }
    }

    private void verifyUsername(User user, BindingResult result) {
        if (userService.findByUsername(user.getUsername()) != null) {
            result.addError(new FieldError("user", "username", "Username already taken!"));
        }
    }

    private void verifyPasswords(User user, BindingResult result, String verification) {
        var password = user.getPassword();
        if (verification == null || !verification.equals(password)) {
            result.addError(new ObjectError("user", "Passwords don't match!"));
        }
        if (password == null || password.length() < MIN_PASSWORD_LENGTH) {
            result.addError(new FieldError("user", "password",
                    "Password too short! (needs to be at least " + MIN_PASSWORD_LENGTH + " characters long)"));
        }
    }

    @PostMapping("/signup")
    public String signup(@ModelAttribute("user") @Valid User user, BindingResult result, @RequestParam String verification, Model model) {
        var name = user.getUsername();
        if (name != null)
            name = name.toLowerCase();
        user.setUsername(name);
        verifyUsername(user, result);
        verifyMail(user, result);
        verifyPasswords(user, result, verification);

        if (result.hasErrors()) {
            model.addAttribute("user", user);
            return "signup";
        }
        user = userService.createUser(user);
        Assert.notNull(user, "Couldn't create User");

        model.addAttribute("alerts", new AlertsDTO().addNotification("Successfully signed up!"));
        model.addAttribute("username", user.getUsername());
        return "login";
    }


    @GetMapping("/profile")
    public String profile(Model model, @AuthenticationPrincipal() User principal) {
        //principal may be an old version
        principal = userService.getUser(principal.getId());
        model.addAttribute("user", principal);
        model.addAttribute("showUserData", true);
        return "profile";
    }

    @PostMapping("/profile")
    public String postProfile(@Valid @ModelAttribute("user") User userDto, BindingResult result, @RequestParam String verification, @AuthenticationPrincipal User principal, Model model) {
        if (userDto.getPassword() != null && userDto.getPassword().length() > 0) {
            verifyPasswords(userDto, result, verification);
        } else {
            userDto.setPassword(null);
        }

        if (!result.hasErrors()) {
            userService.updateProfile(principal, userDto);
            model.addAttribute("alerts", new AlertsDTO().addNotification("Saved"));
        }
        return profile(model, principal);
    }

    @GetMapping("/profile/paymentMethods")
    public String paymentMethods(Model model, @AuthenticationPrincipal User principal) {
        //principal may be an old version
        principal = userService.getUser(principal.getId());
        model.addAttribute("user", principal);
        model.addAttribute("showPaymentMethods", true);
        model.addAttribute("newPaymentMethod", new PaymentMethod());
        return "profile";
    }

    @PostMapping("/profile/paymentMethods")
    public String postPaymentMethod(Model model, @AuthenticationPrincipal User principal, @ModelAttribute("newPaymentMethod") @Valid PaymentMethod newPaymentMethod, BindingResult result) {
        if (result.hasErrors()) {
            paymentMethods(model, principal);
            //overwrite newPaymentMethod, so that errors can be displayed
            //principal may be an old version
            principal = userService.getUser(principal.getId());
            model.addAttribute("user", principal);
            model.addAttribute("showPaymentMethods", true);
            model.addAttribute("newPaymentMethod", newPaymentMethod);
            return "profile";
        }
        userService.addPaymentMethod(principal, newPaymentMethod);
        model.addAttribute("alerts", new AlertsDTO().addNotification("Successfully updated payments"));

        return paymentMethods(model, principal);
    }

    @PostMapping("/profile/deletePaymentMethod")
    public String deletePaymentMethod(Model model, @AuthenticationPrincipal User principal, @RequestParam("paymentMethod") long paymentMethodId) {
        try {
            userService.removePaymentMethod(principal, paymentMethodId);
        } catch (SixrrException e) {
            model.addAttribute("alerts", new AlertsDTO().addError(e.getMessage()));
            return paymentMethods(model, principal);
        }

        model.addAttribute("alerts", new AlertsDTO().addNotification("Deleted PaymentMethod"));
        return paymentMethods(model, principal);
    }

    @GetMapping("/users/{name}")
    public String users(Model model, @PathVariable String name, @AuthenticationPrincipal User principal) {
        var user = userService.findByUsername(name);
        if (user == null) {
            model.addAttribute("alerts", new AlertsDTO().addError("Username not found: " + name));
            return "index";
        }
        model.addAttribute("user", user);
        model.addAttribute("userOffers", offerService.getOffersOfUser(user));
        model.addAttribute("showEditLink", principal != null && principal.equals(user));
        return "user";
    }
}
