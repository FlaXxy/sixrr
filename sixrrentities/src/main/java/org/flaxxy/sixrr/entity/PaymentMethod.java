package org.flaxxy.sixrr.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;


@Entity
public class PaymentMethod extends SingleIdEntity<Long> {

    @Column(nullable = false)
    private PaymentSite site;

    //this one authentication string was intended to hold authentication and account information for both Zea and Bogerpay.
    @Column(nullable = false)
    @Pattern(regexp = "DE\\d{20}", message = "Invalid iban")
    private String iban;
    @NotEmpty
    private String username;
    @NotEmpty
    private String password;

    public PaymentSite getSite() {
        return site;
    }

    public void setSite(PaymentSite site) {
        this.site = site;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String authentication) {
        this.iban = authentication;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
