package org.flaxxy.sixrr.dto;


import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Duration;

public class NewOfferDTO implements Serializable {
    @Positive
    private long paymentMethodId;
    @NotNull
    @NotEmpty
    private String title;
    @NotNull
    @NotEmpty
    private String description;
    @NotNull
    @NotEmpty
    private String shortDescription;
    @Min(1)
    private int maxRevisions;
    @NotNull
    @NotEmpty
    private String invoiceUnit;
    @NotNull
    private BigDecimal price;
    @NotNull
    private BigDecimal pricePerUnit;
    @NotNull
    private Duration duration;
    @NotNull
    private Duration durationPerUnit;
    @Min(0)
    private int maxUnits;

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public int getMaxRevisions() {
        return maxRevisions;
    }

    public void setMaxRevisions(int maxRevisions) {
        this.maxRevisions = maxRevisions;
    }

    public String getInvoiceUnit() {
        return invoiceUnit;
    }

    public void setInvoiceUnit(String invoiceUnit) {
        this.invoiceUnit = invoiceUnit;
    }

    public BigDecimal getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(BigDecimal pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public Duration getDurationPerUnit() {
        return durationPerUnit;
    }

    public void setDurationPerUnit(Duration durationPerUnit) {
        this.durationPerUnit = durationPerUnit;
    }

    public int getMaxUnits() {
        return maxUnits;
    }

    public void setMaxUnits(int maxUnits) {
        this.maxUnits = maxUnits;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public long getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(long paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }
}
