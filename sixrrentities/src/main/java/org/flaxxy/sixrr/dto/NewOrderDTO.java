package org.flaxxy.sixrr.dto;

import javax.validation.constraints.PositiveOrZero;
import java.io.Serializable;

public class NewOrderDTO implements Serializable {

    private long offerId;

    @PositiveOrZero
    private int amount;

    private long paymentMethodId;

    private String requestDescription;

    private long[] bambooOrders;

    public NewOrderDTO(long offerId) {
        this.offerId = offerId;
    }

    public NewOrderDTO() {

    }

    public long getOfferId() {
        return offerId;
    }

    public void setOfferId(long value) {
        offerId = value;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int value) {
        amount = value;
    }

    public long getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(long value) {
        paymentMethodId = value;
    }

    public String getRequestDescription() {
        return requestDescription;
    }

    public void setRequestDescription(String requestDescription) {
        this.requestDescription = requestDescription;
    }

    public long[] getBambooOrders() {
        return bambooOrders;
    }

    public void setBambooOrders(long[] bambooOrders) {
        this.bambooOrders = bambooOrders;
    }
}
