package org.flaxxy.sixrr.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;

@Component
public class StringToDurationConverter implements Converter<String, Duration> {

    @Override
    public Duration convert(String s) {
        try {
            return Duration.between(LocalTime.MIN, LocalTime.parse(s.replace('-', '0')));
        } catch (DateTimeParseException ex) {
            return null;
        }
    }
}
