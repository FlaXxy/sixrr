package org.flaxxy.sixrr.controller;

import org.flaxxy.sixrr.dto.AlertsDTO;
import org.flaxxy.sixrr.service.OfferServiceIF;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Controller
public class HomeController {
    private final OfferServiceIF offerService;

    public HomeController(OfferServiceIF offerService) {
        this.offerService = offerService;
    }

    @GetMapping({"/", "/index"})
    public String index(Model model) {
        var offers = StreamSupport.stream(offerService.getRandomOffers().spliterator(), false).limit(10).collect(Collectors.toList());
        model.addAttribute("offers", offers);
        return "index";
    }

    @GetMapping(value = {"/", "/index"}, params = {"logout"})
    public String indexLogout(Model model) {
        model.addAttribute("alerts", new AlertsDTO().addNotification("Successfully logged out!"));
        return "index";
    }

    @GetMapping(value = "/login", params = {"error"})
    public String loginError(Model model) {
        model.addAttribute("alerts", new AlertsDTO().addError("Invalid username or password!"));
        return "login";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

}
