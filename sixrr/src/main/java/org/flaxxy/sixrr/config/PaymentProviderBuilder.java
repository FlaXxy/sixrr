package org.flaxxy.sixrr.config;

import de.othr.bor.bogerpay.dto.TransactionDTO;
import org.flaxxy.sixrr.entity.PaymentSite;
import org.flaxxy.sixrr.service.payment.PaymentProviderIF;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;

import static org.flaxxy.sixrr.config.CommunicationConfig.BOGERPAY_ADDRESS;

@Configuration
public class PaymentProviderBuilder {


    @Bean("paymentProviders")
    public HashMap<PaymentSite, PaymentProviderIF> build() {
        var result = new HashMap<PaymentSite, PaymentProviderIF>();
        result.put(PaymentSite.paying123, (username, password, senderIban, receiverIban, amount, reference, restTemplate) -> true);
        result.put(PaymentSite.zea, (username, password, senderIban, receiverIban, amount, reference, restTemplate) -> {
            var success = restTemplate.postForEntity("http://192.168.178.40:8080/rest/transaction?sender={sender}&receiver={receiver}&amount={amount}", null, boolean.class,
                    senderIban, receiverIban, amount);
            return success.getStatusCode().is2xxSuccessful();
        });
        result.put(PaymentSite.bogerpay, (username, password, senderIban, receiverIban, amount, reference, restTemplate) -> {
            var dto = new TransactionDTO();
            dto.setAmount(amount.doubleValue());
            dto.setConsignorIBAN(senderIban);
            dto.setRecipientIBAN(receiverIban);
            dto.setReference(reference);
            var success = restTemplate.postForEntity(BOGERPAY_ADDRESS + "/api/transaction?email={email}&password={password}", dto, TransactionDTO.class,
                    username, password);
            return success.getStatusCode().is2xxSuccessful() && success.getBody() != null;
        });
        return result;
    }
}
