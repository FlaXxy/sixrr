package org.flaxxy.sixrr.converter;

import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class TimeFormatter {
    private TimeFormatter() {

    }

    public static String format(@org.jetbrains.annotations.NotNull Duration duration) {
        var secs = duration.getSeconds();
        return String.format("%d:%02d h", secs / 3600, (secs % 3600) / 60);
    }

    public static String format(Instant instant) {
        return DateTimeFormatter.RFC_1123_DATE_TIME.withZone(ZoneId.systemDefault()).format(instant);
    }
}
