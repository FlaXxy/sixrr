package org.flaxxy.sixrr.entity;

import org.flaxxy.sixrr.entity.validation.OrderAmountConstraint;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;
import java.time.Duration;
import java.util.Set;

@Entity
@OrderAmountConstraint
public class ServiceOrder extends SingleIdEntity<Long> {
    @ManyToOne(optional = false)
    @NotNull
    private ServiceOffer offer;

    @ManyToOne(optional = false)
    @NotNull
    private User buyer;

    @ManyToOne
    @NotNull
    private PaymentMethod paymentMethod;

    @OneToMany
    private Set<Revision> revisions;

    @PositiveOrZero
    private int amount;

    @Enumerated(EnumType.ORDINAL)
    private OrderStatus status;

    private String requestDescription;

    private String bambooOrderInfo;

    public Set<Revision> getRevisions() {
        return revisions;
    }

    public void setRevisions(Set<Revision> revisions) {
        this.revisions = revisions;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public ServiceOffer getOffer() {
        return offer;
    }

    public void setOffer(ServiceOffer offer) {
        this.offer = offer;
    }

    public BigDecimal getFinalPrice() {
        return offer.getPrice().add(offer.getPricePerUnit().multiply(BigDecimal.valueOf(getAmount())));
    }

    public User getBuyer() {
        return buyer;
    }

    public void setBuyer(User buyer) {
        this.buyer = buyer;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getRequestDescription() {
        return requestDescription;
    }

    public void setRequestDescription(String requestDescription) {
        this.requestDescription = requestDescription;
    }

    public String getBambooOrderInfo() {
        return bambooOrderInfo;
    }

    public void setBambooOrderInfo(String bambooOrderInfo) {
        this.bambooOrderInfo = bambooOrderInfo;
    }

    public Duration getTotalDuration() {
        return offer.getDuration().plus(offer.getDurationPerUnit().multipliedBy(amount));
    }

    public BigDecimal getTotalPrice() {
        return offer.getPrice().add(offer.getPricePerUnit().multiply(BigDecimal.valueOf(amount)));
    }
}
