package org.flaxxy.sixrr.entity.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = OrderAmountValidator.class)
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface OrderAmountConstraint {
    String message() default "Order amount is more than the max.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
