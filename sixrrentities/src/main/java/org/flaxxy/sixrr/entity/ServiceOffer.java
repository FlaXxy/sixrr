package org.flaxxy.sixrr.entity;


import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Duration;

@Entity
public class ServiceOffer extends SingleIdEntity<Long> {

    @ManyToOne(optional = false)
    private User seller;
    @ManyToOne(optional = false)
    private PaymentMethod paymentMethod;

    @NotNull
    @NotEmpty
    private String title;
    @NotNull
    @NotEmpty
    private String description;
    @NotNull
    @NotEmpty
    private String shortDescription;
    @Min(1)
    private int maxRevisions;
    @NotNull
    @NotEmpty
    private String invoiceUnit;
    @NotNull
    private BigDecimal price;
    @NotNull
    private BigDecimal pricePerUnit;
    @NotNull
    private Duration duration;
    @NotNull
    private Duration durationPerUnit;
    @Min(0)
    private int maxUnits;

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public int getMaxRevisions() {
        return maxRevisions;
    }

    public void setMaxRevisions(int maxRevisions) {
        this.maxRevisions = maxRevisions;
    }

    public String getInvoiceUnit() {
        return invoiceUnit;
    }

    public void setInvoiceUnit(String invoiceUnit) {
        this.invoiceUnit = invoiceUnit;
    }

    public BigDecimal getPricePerUnit() {
        return pricePerUnit;
    }

    public void setPricePerUnit(BigDecimal pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public Duration getDurationPerUnit() {
        return durationPerUnit;
    }

    public void setDurationPerUnit(Duration durationPerUnit) {
        this.durationPerUnit = durationPerUnit;
    }

    public int getMaxUnits() {
        return maxUnits;
    }

    public void setMaxUnits(int maxUnits) {
        this.maxUnits = maxUnits;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public User getSeller() {
        return seller;
    }

    public void setSeller(User seller) {
        this.seller = seller;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
}
