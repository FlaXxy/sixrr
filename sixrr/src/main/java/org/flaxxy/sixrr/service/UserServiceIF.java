package org.flaxxy.sixrr.service;


import org.flaxxy.sixrr.entity.PaymentMethod;
import org.flaxxy.sixrr.entity.User;
import org.flaxxy.sixrr.utils.SixrrException;

import javax.transaction.Transactional;

public interface UserServiceIF {
    @Transactional
    User findByEmail(String mail);

    @Transactional
    User findByUsername(String name);

    @Transactional
    User getUser(long id);

    @Transactional
    User createUser(User data);

    @Transactional
    User getAuthenticatedUser(String username, String password);

    @Transactional
    void updateProfile(User user, User data);

    @Transactional
    void addPaymentMethod(User user, PaymentMethod paymentMethod);

    @Transactional
    void removePaymentMethod(User user, long paymentMethodId) throws SixrrException;

}
