package org.flaxxy.sixrr.repository;

import org.flaxxy.sixrr.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
@Repository
public interface UserRepository extends CrudRepository<User,Long> {

    @Query("SELECT u FROM User u WHERE u.email = ?1")
    Collection<User> findByEmail(String mail);

    @Query("SELECT u FROM User u WHERE u.username = ?1")
    Collection<User> findByUsername(String name);
}
