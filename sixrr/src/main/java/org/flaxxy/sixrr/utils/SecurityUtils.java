package org.flaxxy.sixrr.utils;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.security.SecureRandom;

@Component
public class SecurityUtils {

    private static final String salt = "MQXJyXlTB0q95weoOXVb";

    private final PasswordEncoder encoder = new BCryptPasswordEncoder(11, new SecureRandom(salt.getBytes()));

    public PasswordEncoder getEncoder() {
        return encoder;
    }

}
