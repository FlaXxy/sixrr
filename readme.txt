- Nathalie Holler couldn't make it, so the integration with Zea couldn't be tested.
- I used constructor injection where possible. ( https://www.google.com/search?q=constructor+vs+field+injection )
- /profile is for the active user; /users/id is for any user
- <user>:<password> are:
    - admin:admin
    - user:user
    - greim:greim

URLs:
/profile/ #own user
/profile/paymentmethods #own paymentmethods

/orders/id #own orders
/tasks/id #own task

/users/username #other user
/offers/id #any offers

Paying with paying123 always succeeds.

Not in the scope:
- handling of deleted Paymentmethods, when they were already used
- seller rating
- handling of artifact storage (like pictures, texts, audio files)
- distributed transaction when sending money from buyer to seller, bambooShop and me.
- review of orders

How to open in IntelliJ:
- Open the pom.xml as Maven Project
- add sixxrentities, BambooForestStoreEntities and bogerpay.entites as new modules (pom.xml, too)

Interaction with other projects:
- when a user wants a special transportation (big object) or fast transport in greimDelivery,
greimDelivery orders a transportation service from sixxr.